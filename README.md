# osu-web enhanced

# Features:

General:
- Display the existance (and number of) notifications in the page title

Profile:
- Option to expand me! sections

Forum:
- Add more BBCode quickinsert buttons
- Add a message to every post to directly contact the creator
- Option to highlight own name everywhere

Beatmaps:
- Add a button to get the currently open difficulty's .osu file
- Option to expand beatmap description

Chat:
- Add chat templates
    - Create, Edit, Delete
    - Support for variables
    - Import/Export them for easy sharing