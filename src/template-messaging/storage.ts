import { gmDeleteValue, gmGetValue, gmListValues, gmSetValue } from "../../typescript-lib/src/shared/greasemonkey"
import { templateKeys } from "../constants/constants"
import { CustomMessage, customMessageTypes } from "../constants/types"

/**
 * Gets a sorted list of messages from local storage.
 * @param customMessageType type of custom message @see customMessageTypes
 */
export function getSortedCustomMessages(customMessageType: customMessageTypes) {
    const messages: CustomMessage[] = []

    getCustomMessagesKeys(customMessageType).forEach((key) => {
        messages.push(readCustomMessage(key))
    })

    if (messages.length == 0) {
        messages.push(readCustomMessage(`customMessage-fallback`))
    }

    return messages.sort((a: CustomMessage, b: CustomMessage) => {
        return a.messageOptions.title > b.messageOptions.title ? 1 : -1
    })
}

/**
 * Get storageKeys of all custom messages of given type
 * @param customMessageType type of custom message @see customMessageTypes
 * @returns string[] of storageKeys
 */
export function getCustomMessagesKeys(customMessageType: customMessageTypes) {
    const keyMatch = `${templateKeys[customMessageType]}-[0-9]+$`
    let messagesStorageKeys: string[] = []

    gmListValues().forEach((v) => {
        if (v.match(keyMatch)) {
            messagesStorageKeys.push(v)
        }
    })

    return messagesStorageKeys
}

/**
 * Generates the storageKey for a customMessage
 * @param customMessageType type of custom message @see customMessageTypes
 * @returns storagekey for customMessage.storageKey
 */
export function generateCustomMessageStorageKey(customMessageType: customMessageTypes) {
    return `${templateKeys[customMessageType]}-${Date.now()}`
}

/**
 * Reads a customMessage from storage
 * @param storageKey key the message is stored under
 * @returns CustomMessage @see CustomMessage
 */
export function readCustomMessage(storageKey: string) {
    return JSON.parse(gmGetValue(storageKey) as string) as CustomMessage
}

/**
 * Saves a customMessage to storage
 * @param customMessage customMessage to be saved
 */
export function saveCustomMessage(customMessage: CustomMessage) {
    gmSetValue(customMessage.storageKey, JSON.stringify(customMessage))
}

/**
 * Deletes a customMessage from storage
 * @param customMessage customMessage to be deleted
 */
export function deleteCustomMessage(customMessage: CustomMessage) {
    gmDeleteValue(customMessage.storageKey)
}
