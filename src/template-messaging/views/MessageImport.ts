import { ModalWrapper } from "../../../typescript-lib/src/components/modal/ModalWrapper"
import { OsuModal } from "../../../typescript-lib/src/components/modal/osu/OsuModal"
import { OsuModalButton } from "../../../typescript-lib/src/components/modal/osu/OsuModalButton"
import { OsuModalHeader } from "../../../typescript-lib/src/components/modal/osu/OsuModalHeader"
import { wait } from "../../../typescript-lib/src/shared/common"
import { ModalNotificationArea } from "../../components/modal-notification-area"
import { CustomMessage, customMessageTypes } from "../../constants/types"
import { createTextAreaInput } from "../../shared/dom-styled"
import { saveCustomMessage } from "../storage"
import { createMessagesManager } from "./MessageManager"

/**
 * This function create the modal for importing a message.
 * @param modalWrapper modalwrapper to be inserted into
 * @param customMessageType
 * @returns modal
 */
export function createMessageImport(modalWrapper: ModalWrapper, customMessageType: customMessageTypes) {
    const modal = new OsuModal(modalWrapper, new OsuModalHeader("Import Message"))
    const notificationArea = new ModalNotificationArea("normal", "Please enter the message.")
    const textArea = createTextAreaInput("textarea", "")

    modal.addModalButton(
        new OsuModalButton(
            "Back",
            "back",
            () => {
                modal.replaceWith(createMessagesManager(modalWrapper, customMessageType))
            },
            "secondary"
        )
    )

    modal.addModalButton(
        new OsuModalButton(
            "Save Message",
            "save",
            async () => {
                // save
                saveCustomMessage(JSON.parse(`${textArea.value}`) as CustomMessage)
                notificationArea.updateMessage("normal", "Saved!")
                // reset
                textArea.value = ""
                await wait(3000)
                notificationArea.updateMessage("normal", "Please enter the message.")
            },
            "primary"
        )
    )

    modal.addContent(textArea)
    modal.addContent(notificationArea)

    return modal
}
