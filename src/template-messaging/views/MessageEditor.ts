import { ModalWrapper } from "../../../typescript-lib/src/components/modal/ModalWrapper"
import { OsuModal } from "../../../typescript-lib/src/components/modal/osu/OsuModal"
import { OsuModalButton } from "../../../typescript-lib/src/components/modal/osu/OsuModalButton"
import { OsuModalHeader } from "../../../typescript-lib/src/components/modal/osu/OsuModalHeader"
import { createElement } from "../../../typescript-lib/src/shared/dom"
import { OsuInputText } from "../../components/input-text"
import { MessageEditorCharacterCount } from "../../components/message-editor-character-count"
import { CustomMessageParameterInput } from "../../components/message-editor-parameter-input"
import { ModalNotificationArea } from "../../components/modal-notification-area"
import { CustomMessage, customMessageTypes, editorActionType } from "../../constants/types"
import { createTextAreaInput } from "../../shared/dom-styled"
import { checkParameterInputState, getParametersFromInputs } from "../parameters"
import { saveCustomMessage } from "../storage"
import { createMessagesManager } from "./MessageManager"

/**
 * This function creates the edit view for custom messages
 * @param modalWrapper modalwrapper to be inserted into
 * @param customMessage customMessage to edit, @see CustomMessage
 * @param actionType type of edit action, @see editorActionType
 * @returns modal
 */
export function createMessageEditor(modalWrapper: ModalWrapper, customMessage: CustomMessage, actionType: editorActionType, customMessageType: customMessageTypes) {
    const modalHeader = new OsuModalHeader(actionType == "new" ? "New Message" : "Edit Message")
    const newMessageModal = new OsuModal(modalWrapper, modalHeader)
    newMessageModal.style.minWidth = "700px"

    // create top information
    const notificationArea = new ModalNotificationArea(actionType == "new" ? "danger" : "normal", actionType == "new" ? "Unsaved Changes" : "Everything alright")
    const information = createElement("div", {
        style: {
            display: "flex",
            alignItems: "center",
        },
        children: [notificationArea],
    })
    // always create counter to have access in the event listnerers.
    const characterCounter = new MessageEditorCharacterCount(customMessage.messageOptions.message.length)
    characterCounter.style.marginLeft = "8px"
    if (customMessageType == "chat") {
        information.prepend(characterCounter)
    }

    // create title input
    const container = createElement("div", {
        style: {
            display: "flex",
            alignItems: "center",
        },
    })

    const titleInput = new OsuInputText("please insert message name")
    titleInput.style.width = "100%"
    titleInput.value = actionType == "new" ? "" : customMessage.messageOptions.title
    titleInput.addEventListener("input", () => {
        customMessage.messageOptions.title = titleInput.value
        notificationArea.updateMessage("danger", "Unsaved Changes")
    })
    container.append(
        createElement("span", {
            attributes: {
                innerText: "Name",
            },
            style: {
                fontSize: "14px",
                marginLeft: "8px",
                minWidth: "52px",
            },
        }),
        titleInput
    )

    // create message write area
    const messageArea = createTextAreaInput("textarea", actionType == "new" ? "" : customMessage.messageOptions.message)
    messageArea.rows = 7
    messageArea.addEventListener("input", () => {
        customMessage.messageOptions.message = messageArea.value
        if (customMessageType == "chat") {
            characterCounter.updateCounter(messageArea.value.length)
        }
        notificationArea.updateMessage("danger", "Unsaved Changes")
    })

    // append items
    newMessageModal.addContent(container)
    newMessageModal.addContent(information)
    newMessageModal.addContent(messageArea)

    // append parameter inputs
    customMessage.messageOptions.parameters.forEach((p) => {
        messageArea.after(new CustomMessageParameterInput(notificationArea, "edit", p))
    })

    // add buttons
    newMessageModal.addModalButton(
        new OsuModalButton(
            "Back",
            "back",
            () => {
                newMessageModal.replaceWith(createMessagesManager(modalWrapper, customMessageType))
            },
            "secondary"
        )
    )
    newMessageModal.addModalButton(
        new OsuModalButton(
            "Save",
            "save",
            () => {
                if (customMessage.messageOptions.message !== "" && customMessage.messageOptions.title !== "" && checkParameterInputState(newMessageModal)) {
                    customMessage.messageOptions.parameters = getParametersFromInputs(newMessageModal)
                    saveCustomMessage(customMessage)
                    notificationArea.updateMessage("normal", "Everything Alright")
                } else {
                    notificationArea.updateMessage("danger", "Please fill out all fields!")
                }
            },
            "primary"
        )
    )
    newMessageModal.addModalButton(
        new OsuModalButton(
            "New Variable",
            "new-variable",
            () => {
                messageArea.after(new CustomMessageParameterInput(notificationArea, "new"))
            },
            "primary"
        )
    )

    return newMessageModal
}
