import { ModalWrapper } from "../../../typescript-lib/src/components/modal/ModalWrapper"
import { OsuModal } from "../../../typescript-lib/src/components/modal/osu/OsuModal"
import { OsuModalButton } from "../../../typescript-lib/src/components/modal/osu/OsuModalButton"
import { OsuModalHeader } from "../../../typescript-lib/src/components/modal/osu/OsuModalHeader"
import { wait } from "../../../typescript-lib/src/shared/common"
import { ModalNotificationArea } from "../../components/modal-notification-area"
import { CustomMessage, customMessageTypes } from "../../constants/types"
import { createTextAreaInput } from "../../shared/dom-styled"
import { generateCustomMessageStorageKey } from "../storage"
import { createMessagesManager } from "./MessageManager"

/**
 * This function create the modal for exporting a message.
 * @param modalWrapper modalwrapper to be inserted into
 * @param customMessage customMessage to export, @see CustomMessage
 * @param customMessageType
 * @returns modal
 */
export function createMessageExport(modalWrapper: ModalWrapper, customMessage: CustomMessage, customMessageType: customMessageTypes) {
    customMessage.storageKey = generateCustomMessageStorageKey(customMessageType)
    const modal = new OsuModal(modalWrapper, new OsuModalHeader("Export Message"))
    const notificationArea = new ModalNotificationArea("danger", "Do not change any of this unless you know what you are doing!")
    const textArea = createTextAreaInput("textarea", JSON.stringify(customMessage))

    modal.addModalButton(
        new OsuModalButton(
            "Back",
            "back",
            () => {
                modal.replaceWith(createMessagesManager(modalWrapper, customMessageType))
            },
            "secondary"
        )
    )

    modal.addModalButton(
        new OsuModalButton(
            "Copy",
            "copy",
            async () => {
                await navigator.clipboard.writeText(textArea.value)
                notificationArea.updateMessage("normal", "Copied")
                await wait(3000)
                notificationArea.updateMessage("danger", "Do not change any of this unless you know what you are doing!")
            },
            "primary"
        )
    )

    modal.addContent(textArea)
    modal.addContent(notificationArea)

    return modal
}
