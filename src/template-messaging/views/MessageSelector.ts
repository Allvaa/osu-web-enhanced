import { ModalWrapper } from "../../../typescript-lib/src/components/modal/ModalWrapper"
import { OsuModal } from "../../../typescript-lib/src/components/modal/osu/OsuModal"
import { OsuModalButton } from "../../../typescript-lib/src/components/modal/osu/OsuModalButton"
import { OsuModalHeader } from "../../../typescript-lib/src/components/modal/osu/OsuModalHeader"
import { sendChatMessage } from "../../chat"
import { MessageSelectOption } from "../../components/chat-message-option"
import { OsuInputDropdown } from "../../components/input-dropdown"
import { customMessageTypes } from "../../constants/types"
import { insertForumMessage } from "../../forum"
import { getSortedCustomMessages } from "../storage"
import { createMessagesManager } from "./MessageManager"

/**
 * This function creates the message selector modal
 * @param modalWrapper
 * @param customMessageType
 * @return modal
 */
 export function createMessageSelectModal(modalWrapper: ModalWrapper, customMessageType: customMessageTypes) {
    const modalHeader = new OsuModalHeader("Select Message")
    modalHeader.addIconButton("edit", "Manage Messages", () => {
        modal.replaceWith(createMessagesManager(modalWrapper, customMessageType))
    })
    const modal = new OsuModal(modalWrapper, modalHeader)

    const selector = new OsuInputDropdown()
    getSortedCustomMessages(customMessageType).forEach((customMessage) => {
        selector.appendOption(new MessageSelectOption(customMessage))
    })

    modal.addContent(selector)

    switch (customMessageType) {
        case "chat":
            modal.addModalButton(
                new OsuModalButton(
                    "Send",
                    "send",
                    () => {
                        sendChatMessage(getSelectedOption())
                    },
                    "primary"
                )
            )
            break
        case "forum":
            modal.addModalButton(
                new OsuModalButton(
                    "Insert",
                    "insert-template",
                    () => {
                        insertForumMessage(getSelectedOption())
                    },
                    "primary"
                )
            )
            break
    }

    return modal

    function getSelectedOption() {
        return (selector.getSelector().options[selector.getSelector().selectedIndex] as MessageSelectOption).getCustomMessage()
    }
}