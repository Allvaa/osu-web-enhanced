import { ModalWrapper } from "../typescript-lib/src/components/modal/ModalWrapper"
import { ensuredSelector, wait } from "../typescript-lib/src/shared/common"
import { createElement, createIcon, waitForElement } from "../typescript-lib/src/shared/dom"
import { settings } from "./constants/constants"
import { CustomMessage } from "./constants/types"
import { useFeature } from "./settings"
import { insertParametersIntoCustomMessage } from "./template-messaging/parameters"
import { createMessageSelectModal } from "./template-messaging/views/MessageSelector"

/**
 * Insert chat manager button
 */
export async function chatShortcuts() {
    const target = await waitForElement<HTMLElement>(".chat-input")
    target.style.paddingLeft = "30px"

    do {
        useFeature(settings.general.showMessageManager.storageKey, () => {
            if (document.querySelector(".rr-lib.btn-osu-big.btn-osu-big--chat-templates") !== null) {
                return
            }
            target.prepend(createChatTemplatesButton())
        })

        await wait(2000)
    } while (location.pathname.includes("chat"))
}

/**
 * Creates chat manager button
 * @returns button
 */
function createChatTemplatesButton() {
    return createElement("button", {
        className: "btn-osu-big btn-osu-big--chat-templates",
        style: {
            marginRight: "10px",
            borderRadius: "10000px",
            padding: "9px",
        },
        children: [
            createElement("span", {
                className: "btn-osu-big__content",
                children: [
                    createElement("span", {
                        className: "btn-osu-big__icon",
                        children: [createIcon("collection", { size: 20, display: "flex", alignItems: "center" })],
                    }),
                ],
            }),
        ],
        attributes: {
            onclick: () => {
                const modalWrapper = new ModalWrapper()
                modalWrapper.append(createMessageSelectModal(modalWrapper, "chat"))
                ensuredSelector("body").append(modalWrapper)
            },
        },
    })
}

/**
 * This function sends a message in the currently selected chat.
 * @param message The message to be sent in the chat.
 */
export function sendChatMessage(customMessage: CustomMessage) {
    const chatInput = ensuredSelector<HTMLTextAreaElement>(".chat-input__box")
    const formattedInput = insertParametersIntoCustomMessage(customMessage)

    if (window.confirm(`Are you sure you want to send this message to ${getSelectedChatUser()}`)) {
        // ! is bad, but I have no clue how to fix it here, since this code is based on some old code.
        const nativeIn = Object.getOwnPropertyDescriptor(window.HTMLTextAreaElement.prototype, "value")!.set

        nativeIn!.call(chatInput, formattedInput)
        chatInput.dispatchEvent(new Event("input", { bubbles: true }))
        ensuredSelector<HTMLButtonElement>(".btn-osu-big.btn-osu-big--chat-send").click()
        ensuredSelector<HTMLInputElement>(".chat-input__box").disabled = false
    }
}

/**
 * This function returns the currently selected userchat.
 * @returns The currently selected user.
 */
export function getSelectedChatUser() {
    return ensuredSelector<HTMLElement>(".chat-conversation-list-item.chat-conversation-list-item--selected .chat-conversation-list-item__name").innerText
}
