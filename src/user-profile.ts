import { ensuredSelector, wait } from "../typescript-lib/src/shared/common"
import { waitForElement } from "../typescript-lib/src/shared/dom"
import { UserProfileMeExpander } from "./components/user-profile-me-expander"
import { settings } from "./constants/constants"
import { useFeature } from "./settings"

/**
 * This functions intitialises all the userpage related modifications.
 */
export async function insertUserpageModifications() {
    await waitForElement(".page-extra--userpage")

    do {
        useFeature(settings.general.showExpandMe.storageKey, insertMeSectionExpander)

        await wait(2000)
    } while (location.pathname.split("/")[1] === "users")
}

/**
 * inserts the me! section expander
 * @returns
 */
function insertMeSectionExpander() {
    if (document.querySelector(".me-expander") != null) {
        return
    }

    const userpage = ensuredSelector<HTMLElement>(".page-extra--userpage")
    userpage.style.paddingBottom = "35px"
    userpage.append(new UserProfileMeExpander())

    // add eventListener to edit button to reset expander state
    const button = ensuredSelector(".page-extra--userpage .btn-circle--page-toggle")
    if (button.classList.contains("edited")) {
        return
    }
    button.addEventListener("click", () => {
        document.querySelector<UserProfileMeExpander>(".expander")?.toggleState("collapse")
    })
    button.classList.add("edited")
}
