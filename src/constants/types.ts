/**
 * Different types of buttons, defined by osu-web.
 * primary = green
 * secondary = blue
 */
export type buttonType = "primary" | "secondary"

/**
 * Defines the various options for a message.
 * @param message the message of the message.
 * @param title the title of the message.
 * @param selected if true this element will be the by default selected one in dropdowns
 * @param parameters replacement parameters for param message
 */
export type MessageOptions = {
    message: string
    title: string
    selected?: boolean
    parameters: Parameter[]
}

/**
 * Defines a custom message.
 * @param messageOptions @see MessageOptions
 * @param storageKey key under which this message is stored.
 */
export type CustomMessage = {
    messageOptions: MessageOptions
    storageKey: string
}

/**
 * Defines a replacement parameter.
 * @param label the visual name of the parameter
 * @param parameter the parameter to use for replacement
 */
export type Parameter = {
    label: string
    parameter: string
}

/**
 * Notification type for ChatNotificationArea
 */
export type notificationType = "danger" | "normal"

/**
 * Action type for ChatManager edit/create.
 */
export type editorActionType = "edit" | "new"

/**
 * Areas that custom messages are used in
 */
export type customMessageTypes = "chat" | "forum"

/**
 * detailed info about beatmap metadata
 */
export type beatmapInfo = {
    artist: string
    artist_unicode: string
    source: string
    tags: string
    title: string
    title_unicode: string
    id: number
    video: boolean
}

/**
 * detailed info about beatmap discussions
 */
export type discussionInfo = {
    beatmapset: beatmapInfo & {
        beatmaps: beatmap[]
    }
}

/**
 * detailed info about a beatmap
 */
export type beatmap = {
    id: number
}
