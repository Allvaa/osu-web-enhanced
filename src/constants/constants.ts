/**
 * This object defines all the aditional bbcode options and what icon should be used for them.
 * The key is the BBCode tag, the value the icon
 */
export const additionalBBCode = {
    u: "fas fa-underline",
    color: "fas fa-paint-brush",
    spoiler: "fas fa-stream",
    quote: "fas fa-quote-right",
    centre: "fas fa-align-center",
    youtube: "fab fa-youtube",
    audio: "fas fa-music",
    notice: "fas fa-comment-alt",
    code: "fas fa-code",
}

/**
 * This object defines all the settings for the script.
 * Settings are grouped into categories according to the area they are used in.
 * All settings need to have a storageKey, label and default.
 * @param storageKey key under which this setting is stored inside GM.
 * @param label visual label of the setting.
 * @param default determines if this setting is enabled by default
 */
export const settings = {
    general: {
        showNotification: {
            storageKey: `settings-showNotification`,
            label: `Show in the tab bar that there are new notifications.`,
            default: true,
        },
        showExpandMe: {
            storageKey: `settings-showExpandMe`,
            label: `Show button to expand me! pages.`,
            default: true,
        },
        showMessageManager: {
            storageKey: `settings-showChatManager`,
            label: `Show chat manager.`,
            default: true,
        },
        logAPIdata: {
            storageKey: `settings-logAPIdata`,
            label: `Log API data to console on all pages.`,
            default: false,
        },
        openNonPPYLinksExternal: {
            storageKey: `settings-openNonPPYLinksExternal`,
            label: `Open all links that aren't on ppy.sh in a new tab.`,
            default: true,
        },
    },
    forum: {
        showSendMessage: {
            storageKey: `settings-showSendMessage`,
            label: `Enable send message buttons.`,
            default: true,
        },
        showAdditionalBBCode: {
            storageKey: `settings-showAdditionalBBCode`,
            label: `Show additional BBCode buttons.`,
            default: false,
        },
        highlightOwnName: {
            storageKey: `settings-highlightOwnName`,
            label: `Highlight own name on forum listing and threads.`,
            default: false,
        },
        insertQuoteAtCursorPosition: {
            storageKey: `settings-insertQuoteAtCursorPosition`,
            label: `Insert quotes at current cursor position in topic reply box.`,
            default: true,
        },
        showMessageManager: {
            storageKey: `settings-showForumManager`,
            label: `Show forum template manager.`,
            default: true,
        },
    },
    beatmaps: {
        showDotOsuData: {
            storageKey: `settings-showDotOsuData`,
            label: `Show .osu data shortcut.`,
            default: true,
        },
        showExpandDescription: {
            storageKey: `settings-showExpandDescription`,
            label: `Show button to expand descriptions`,
            default: true,
        },
        showDetailedInfo: {
            storageKey: `settings-showDetailedInfo`,
            label: `Show button to display detailed info`,
            default: true,
        },
        showDownloadOnDiscussion: {
            storageKey: `settings-showDownloadOnDiscussion`,
            label: `Show download buttons on discussion page`,
            default: true,
        },
    },
}

/**
 * map of all the keys for customMessages
 * would just use the type-template, but that wasn't used for chat so i chose this approach rather than having to reformat all messages
 */
export const templateKeys = {
    forum: "forum-template",
    chat: "message",
}
