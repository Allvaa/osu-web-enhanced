import { ToggleInput } from "../typescript-lib/src/components/inputs/Toggle"
import { BaseModal } from "../typescript-lib/src/components/modal/base/BaseModal"
import { BaseModalButton } from "../typescript-lib/src/components/modal/base/BaseModalButton"
import { BaseModalHeader } from "../typescript-lib/src/components/modal/base/BaseModalHeader"
import { ModalWrapper } from "../typescript-lib/src/components/modal/ModalWrapper"
import { OsuModal } from "../typescript-lib/src/components/modal/osu/OsuModal"
import { OsuModalButton } from "../typescript-lib/src/components/modal/osu/OsuModalButton"
import { OsuModalHeader } from "../typescript-lib/src/components/modal/osu/OsuModalHeader"
import { insertBeatmapModifications } from "./beatmap"
import { chatShortcuts } from "./chat"
import { MessageSelectOption } from "./components/chat-message-option"
import { OsuInputDropdown } from "./components/input-dropdown"
import { OsuInputLabel } from "./components/input-label"
import { OsuInputText } from "./components/input-text"
import { MessageEditorCharacterCount } from "./components/message-editor-character-count"
import { MessageEditorListItem } from "./components/message-editor-list-item"
import { CustomMessageParameterInput } from "./components/message-editor-parameter-input"
import { ModalNotificationArea } from "./components/modal-notification-area"
import { UserProfileMeExpander } from "./components/user-profile-me-expander"
import { settings } from "./constants/constants"
import { insertForumModifications } from "./forum"
import { updateNotificationDisplay } from "./notifications"
import { initSettings, insertSettingsModalOpenButton, useFeature } from "./settings"
import { insertUserpageModifications } from "./user-profile"

// ==UserScript==
// @name osu-web enhanced
// @version  1.2.1
// @author RockRoller
// @match https://osu.ppy.sh/*
// @grant GM_setValue
// @grant GM_getValue
// @grant GM_deleteValue
// @grant GM_listValues
// @downloadURL https://gist.github.com/RockRoller01/e0e10ff9e5716701e4a0b54f6bcddf42/raw/script.user.js
// @updateURL https://gist.github.com/RockRoller01/e0e10ff9e5716701e4a0b54f6bcddf42/raw/script.user.js
// ==/UserScript==

const routes = [
    {
        match: ["forums"],
        render: () => insertForumModifications(),
    },
    {
        match: ["beatmapsets"],
        render: () => insertBeatmapModifications(),
    },
    {
        match: ["chat"],
        render: () => chatShortcuts(),
    },
    {
        match: ["users"],
        render: () => insertUserpageModifications(),
    },
]

function determineOsuRoute() {
    for (const route of routes) {
        const splitURL = location.pathname.split("/")
        const matches = splitURL.some((u) => route.match.includes(u))
        if (matches) {
            return route.render()
        }
    }
}

function listenForHistoryChange(callback: () => void) {
    let lastLocation = location.pathname

    const fn = () => {
        if (location.pathname !== lastLocation) {
            lastLocation = location.pathname
            callback()
        }

        setTimeout(() => fn(), 50)
    }

    fn()
    callback()
}

async function main() {
    // functions registered in this section will only run once per tab
    initSettings()
    initComponents()

    listenForHistoryChange(async () => {
        // functions registered in this section will run on every navigation
        useFeature(settings.general.logAPIdata.storageKey, logAPIData)
        useFeature(settings.general.openNonPPYLinksExternal.storageKey, () => {
            setInterval(() => openExternalLinksInNewTab(), 2500)
        })
        insertSettingsModalOpenButton()
        determineOsuRoute()

        useFeature(settings.general.showNotification.storageKey, () => {
            setInterval(() => updateNotificationDisplay(), 2500)
        })
    })
}

main()

/**
 * This function registers all components, must be called before using the components
 */
function initComponents() {
    customElements.define("custom-input-toggle-e", ToggleInput, { extends: "div" })
    customElements.define("modal-wrapper-e", ModalWrapper, { extends: "div" })
    customElements.define("base-modal-e", BaseModal, { extends: "div" })
    customElements.define("base-modal-button-e", BaseModalButton, { extends: "button" })
    customElements.define("base-modal-header-e", BaseModalHeader, { extends: "div" })
    customElements.define("osu-modal-e", OsuModal, { extends: "div" })
    customElements.define("osu-modal-button-e", OsuModalButton, { extends: "button" })
    customElements.define("osu-modal-header-e", OsuModalHeader, { extends: "div" })
    customElements.define("chat-character-count-e", MessageEditorCharacterCount, { extends: "div" })
    customElements.define("chat-message-list-item-e", MessageEditorListItem, { extends: "div" })
    customElements.define("message-manager-select-option-e", MessageSelectOption, { extends: "option" })
    customElements.define("user-profile-me-expander-e", UserProfileMeExpander, { extends: "div" })
    customElements.define("osu-input-text-e", OsuInputText, { extends: "input" })
    customElements.define("modal-notification-area-e", ModalNotificationArea, { extends: "div" })
    customElements.define("message-manager-parameter-input-e", CustomMessageParameterInput, { extends: "div" })
    customElements.define("osu-input-dropdown-e", OsuInputDropdown, { extends: "label" })
    customElements.define("osu-input-label-e", OsuInputLabel, { extends: "label" })
}

function logAPIData() {
    document.querySelectorAll<HTMLScriptElement>("script[id*=json]").forEach((e) => {
        console.log(e.id, JSON.parse(e.innerText))
    })
}

function openExternalLinksInNewTab() {
    document.querySelectorAll<HTMLAnchorElement>("a").forEach((a) => {
        if (a.href == "") {
            return
        }
        try {
            const url = new URL(a.href)
            if (url.protocol.includes("http") && !url.host.includes("ppy.sh")) {
                a.target = "_blank"
            }
        } catch (e) {
            console.log(a, a.href)
        }
    })
}
