import { ModalWrapper } from "../typescript-lib/src/components/modal/ModalWrapper"
import { OsuModal } from "../typescript-lib/src/components/modal/osu/OsuModal"
import { OsuModalHeader } from "../typescript-lib/src/components/modal/osu/OsuModalHeader"
import { ensuredSelector, wait } from "../typescript-lib/src/shared/common"
import { createElement, insertStyleTag, waitForElement } from "../typescript-lib/src/shared/dom"
import { settings } from "./constants/constants"
import { beatmapInfo, discussionInfo } from "./constants/types"
import { useFeature } from "./settings"

/**
 * This functions intitialises all the beatmap related modifications.
 */
export async function insertBeatmapModifications() {
    do {
        await wait(500)
        if (!location.pathname.includes("discussion")) {
            await waitForElement("#json-beatmapset")
            useFeature(settings.beatmaps.showDotOsuData.storageKey, insertGetDotOsuButton)
            useFeature(settings.beatmaps.showExpandDescription.storageKey, insertBeatmapDescriptionExpander)
            useFeature(settings.beatmaps.showDetailedInfo.storageKey, insertDetailedInfo)
        } else {
            await waitForElement(".beatmap-discussions-header-bottom__content")
            useFeature(settings.beatmaps.showDownloadOnDiscussion.storageKey, insertDownloadButtonsOnDiscussion)
        }
        await wait(2000)
    } while (location.pathname.split("/")[1] === "beatmapsets")
}

/**
 * adds beatmap dl buttons to the discussion page
 */
function insertDownloadButtonsOnDiscussion() {
    if (document.querySelector(".discussion-download") != null) {
        return
    }

    const target = ensuredSelector(".beatmap-discussions-header-bottom__content")

    const discussionInfo = JSON.parse(ensuredSelector<HTMLScriptElement>("#json-beatmapset-discussion").innerText) as discussionInfo

    const container = createElement("div", {
        className: "beatmap-discussions-header-bottom__details discussion-download",
        style: {
            display: "flex",
            gap: "10px",
            minHeight: "32.5px",
        },
    })

    container.append(createDlButton(`https://osu.ppy.sh/beatmapsets/${discussionInfo.beatmapset.id}/download`, "Download"))
    if (discussionInfo.beatmapset.video) {
        container.append(createDlButton(`https://osu.ppy.sh/beatmapsets/${discussionInfo.beatmapset.id}/download?noVideo=1`, "No Video"))
    }
    container.append(createDlButton(`osu://b/${discussionInfo.beatmapset.beatmaps[0].id}`, "osu!direct"))

    target.append(container)

    function createDlButton(url: string, label: string) {
        const el = createElement("a", {
            className: "btn-osu-big btn-osu-big--full",
            children: [
                createElement("span", {
                    attributes: {
                        innerText: label,
                    },
                    style: {
                        maxWidth: "max-content",
                        paddingLeft: "4px",
                    },
                }),
                createElement("span", { className: "fas fa-download", style: { paddingRight: "10px" } }),
            ],
            attributes: {
                href: url,
            },
            style: {
                padding: "6px",
                display: "flex",
                gap: "4px",
                alignItems: "center",
                justifyContent: "space-between",
            },
        })

        // dont ask me what turbolinks is, but it makes the download work
        el.dataset.turbolinks = "false"

        return el
    }
}

/**
 * This function gets the .osu data for the currently selected difficulty
 */
function insertGetDotOsuButton() {
    if (document.querySelector(".get-osu-data") != null) {
        return
    }
    insertTextButton("get-osu-data", "Get .osu data", () => {
        const dl = createElement("a", {
            attributes: {
                href: `https://osu.ppy.sh/osu/${window.location.toString().split("/")[window.location.toString().split("/").length - 1]}`,
                download: "",
            },
        })
        document.body.append(dl)
        dl.click()
        dl.remove()
    })
}

/**
 * This function inserts a button into the beatmap panel that consists of just text and no fancy button styling
 * @param className class for the button
 * @param label label for the button
 * @param callback function that should be executed onClick
 */
async function insertTextButton(className: string, label: string, callback: Function) {
    const target = await waitForElement(".beatmapset-info__box.beatmapset-info__box--meta")

    const checkDuplicate = Array.from(document.querySelectorAll(`.rr-lib.${className}`))
    if (checkDuplicate.length > 0) {
        checkDuplicate.map((x) => x.remove())
    }

    target.prepend(
        createElement("a", {
            className: className,
            attributes: {
                innerText: label,
                onclick: () => callback(),
            },
            style: {
                cursor: "pointer",
                display: "block",
            },
        })
    )
}

/**
 * inserts a modal that displays detailed metadata info for the beatmap
 */
function insertDetailedInfo() {
    if (document.querySelector(".detailed-info") != null) {
        return
    }
    // log beatmapset api
    const info = JSON.parse(ensuredSelector<HTMLScriptElement>("#json-beatmapset").innerText) as beatmapInfo

    insertTextButton("detailed-info", "View detailed information", () => {
        const modalWrapper = new ModalWrapper()
        const modal = new OsuModal(modalWrapper, new OsuModalHeader("Detailed Metadata Info"))
        modal.style.maxWidth = "40vw"
        modal.style.backgroundColor = "hsl(var(--hsl-b5))"

        modal.addContent(createInfoRow("Artist", info.artist))
        modal.addContent(createInfoRow("Artist (Unicode)", info.artist_unicode))
        modal.addContent(createInfoRow("Source", info.source))
        modal.addContent(createInfoRow("Title", info.title))
        modal.addContent(createInfoRow("Title (Unicode)", info.title_unicode))
        modal.addContent(
            createElement("div", {
                style: {
                    display: "flex",
                    flexDirection: "column",
                    gap: "4px",
                },
                children: [
                    createElement("span", {
                        style: {
                            fontWeight: "bold",
                        },
                        attributes: {
                            innerText: `Tags:`,
                        },
                    }),

                    createElement("div", {
                        style: {
                            display: "flex",
                            gap: "4px",
                            flexWrap: "wrap",
                        },
                        children: [
                            ...info.tags.split(" ").map((tag) => {
                                return createElement("span", {
                                    style: {
                                        padding: "2px 4px",
                                        backgroundColor: "#454f54",
                                        borderRadius: "2px",
                                    },
                                    attributes: {
                                        innerText: tag,
                                    },
                                })
                            }),
                        ],
                    }),
                ],
            })
        )

        modalWrapper.append(modal)
        document.body.append(modalWrapper)
    })

    function createInfoRow(label: string, content: string) {
        return createElement("div", {
            style: {
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                gap: "4px",
            },
            children: [
                createElement("span", {
                    style: {
                        fontWeight: "bold",
                    },
                    attributes: {
                        innerText: `${label}:`,
                    },
                }),
                createElement("span", {
                    attributes: {
                        innerText: content,
                    },
                }),
            ],
        })
    }
}

function insertBeatmapDescriptionExpander() {
    if (document.querySelector(".description-expander") != null) {
        return
    }
    insertTextButton("description-expander", "View full description", () => {
        const modalWrapper = new ModalWrapper()
        const modal = new OsuModal(modalWrapper, new OsuModalHeader("Full Description"))
        modal.style.backgroundColor = "hsl(var(--hsl-b5))"
        modal.addContent(getBeatmapDescription())
        modalWrapper.append(modal)
        document.body.append(modalWrapper)
    })
}

function getBeatmapDescription() {
    const desc = ensuredSelector<HTMLElement>(".beatmapset-info__description .bbcode")

    desc.querySelectorAll<HTMLElement>(".bbcode-spoilerbox:not(.js-spoilerbox--open)").forEach((e) => {
        const closedButton = e.querySelector<HTMLElement>(".bbcode-spoilerbox__link")
        if (closedButton) {
            closedButton.click()
        }
    })

    insertStyleTag(`
        .enhanced-modal .bbcode{
            max-width: 60vw;
            max-height: 80vh;
            overflow-y: auto;
            padding: 0px 8px;
        }    
    `)

    return desc.cloneNode(true) as HTMLElement
}
