import { ModalWrapper } from "../typescript-lib/src/components/modal/ModalWrapper"
import { ensuredSelector, wait } from "../typescript-lib/src/shared/common"
import { createElement, createIcon, insertStyleTag, waitForElement } from "../typescript-lib/src/shared/dom"
import { osuTheme } from "../typescript-lib/src/shared/themes"
import { additionalBBCode, settings } from "./constants/constants"
import { CustomMessage } from "./constants/types"
import { useFeature } from "./settings"
import { insertParametersIntoCustomMessage } from "./template-messaging/parameters"
import { createMessageSelectModal } from "./template-messaging/views/MessageSelector"

/**
 * This function handles all the initialisation action for the forum section of the script.
 */
export async function insertForumModifications() {
    do {
        useFeature(settings.forum.highlightOwnName.storageKey, insertOwnNameHighlighter)
        useFeature(settings.forum.showSendMessage.storageKey, insertSendMessageButton)
        useFeature(settings.forum.showAdditionalBBCode.storageKey, insertAdditionalBBCodeButtons)
        useFeature(settings.forum.insertQuoteAtCursorPosition.storageKey, insertQuoteAtCursorPosition)
        useFeature(settings.forum.showMessageManager.storageKey, insertForumTemplateButton)

        await wait(2500)
    } while (location.pathname.includes("forums"))
}

function insertOwnNameHighlighter() {
    if (document.querySelector("style.rr-lib") != null) {
        return
    }
    insertStyleTag(`
        .osu-page--forum a[href="${document.querySelector<HTMLAnchorElement>(".js-current-user-avatar")?.href}"],
        .osu-page--forum-topic a[href="${document.querySelector<HTMLAnchorElement>(".js-current-user-avatar")?.href}"] {
            color: ${osuTheme.successColor} !important;
            font-weight: 700 !important;
        }
    `)
}

/**
 * This function adds a button to every forum post to directly jump to the chat with the user.
 * Normally one would need to use the user context menu otherwise, which is a bit clunky.
 */
function insertSendMessageButton() {
    Array.from(document.querySelectorAll<HTMLElement>(".forum-post-info")).map((forumPost) => {
        // if there already is a send message button return
        if (forumPost.querySelector(".rr-lib.send-message-button") !== null) {
            return
        }
        try {
            const userID = forumPost.querySelector<HTMLAnchorElement>(".forum-post-info__row.forum-post-info__row--username.js-usercard")!.href.split("https://osu.ppy.sh/users/")[1]

            forumPost.querySelector(".forum-post-info__row--flag")?.append(
                createElement("a", {
                    className: "send-message-button",
                    attributes: {
                        href: `https://osu.ppy.sh/home/messages/users/${userID}`,
                        title: "Send message",
                    },
                    style: {
                        marginLeft: "8px",
                        backgroundColor: osuTheme.modalBackground,
                        borderRadius: "4px",
                        color: osuTheme.normalTextColor,
                        width: "30px",
                        height: "20px",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        boxShadow: "0 1px 3px rgba(0,0,0,.25)",
                    },
                    children: [createIcon("message")],
                })
            )
        } catch {}
    })
}

/**
 * This functions adds additional BBCode shortcut buttons to the text editor.
 * TODO: make it work with all editors (no clue why it doesnt currently)
 */
async function insertAdditionalBBCodeButtons() {
    try {
        await waitForElement(".post-box-toolbar .bbcode-size-select")
    } catch (e) {
        return
    }

    document.querySelectorAll(".bbcode-editor__buttons-bar").forEach((editor) => {
        const target = ensuredSelector(".post-box-toolbar .bbcode-size-select")

        if (editor.querySelector(".btn-circle--bbcode.rr-lib") == null) {
            Object.entries(additionalBBCode).map((option) => {
                target.before(createBBCodeButton(option[0], option[1]))
            })
        }
    })
}

/**
 * This function creates a BBCode shortcut button based on given input.
 * @param name Name of the button/tag
 * @param icon Icon to be used (using FA icons)
 * @returns The button - HTMLButtonElement
 */
function createBBCodeButton(name: string, icon: string) {
    const button = createElement("button", {
        className: `btn-circle btn-circle--bbcode js-bbcode-btn--${name}`,
        attributes: {
            type: "button",
            title: name.charAt(0).toUpperCase() + name.slice(1),
        },
        children: [
            createElement("span", {
                className: `btn-circle__content`,
                children: [
                    createElement("i", {
                        className: icon,
                    }),
                ],
            }),
        ],
    })

    if (name == "color") {
        // @ts-ignore
        const colorPicker = createElement("input", {
            className: "color-picker",
            attributes: {
                type: "color",
                value: "#FF0000",
                onchange: () => insertBBCodeTagIntoTextArea(name, colorPicker.value),
            },
            style: {
                display: "none",
            },
        })

        document.body.append(colorPicker)

        button.addEventListener("click", () => {
            colorPicker.click()
        })
    } else {
        button.addEventListener("click", () => {
            insertBBCodeTagIntoTextArea(name)
        })
    }

    return button
}

/**
 * Causes quote buttons on forum posts to insert their content at the current cursor position in the
 * topic reply box instead of at the end.
 */
function insertQuoteAtCursorPosition() {
    document.querySelectorAll<HTMLElement>(".js-forum-post").forEach((forumPost) => {
        let quoteButton = ensuredSelector<HTMLButtonElement>("div.forum-post__actions button.js-forum-topic-reply--quote", forumPost)

        if (quoteButton.dataset.clickListenerAdded == null) {
            quoteButton.addEventListener("click", async (event) => {
                event.stopImmediatePropagation()
                insertTextIntoBBCodeEditor(await (await fetch(`https://osu.ppy.sh/community/forums/posts/${forumPost.dataset.postId}/raw?quote=1`)).text())
            })
            quoteButton.setAttribute("data-click-listener-added", "")
        }
    })
}

/**
 * This function adds the tag to the text in the editor.
 * @param name Name of the tag
 * @param value Optional value (used for [color=])
 */
function insertBBCodeTagIntoTextArea(name: string, value?: string) {
    const textField = ensuredSelector<HTMLTextAreaElement>(".bbcode-editor__body")
    const selectionStart = textField.selectionStart
    const selectionEnd = textField.selectionEnd

    if (selectionStart == selectionEnd) {
        insertTextIntoBBCodeEditor(`[${name}${value != undefined ? `=${value}` : ""}][/${name}]`)
    } else {
        // value setting
        let selectedText = textField.value.slice(selectionStart, selectionEnd)
        textField.value = `${textField.value.substring(0, selectionStart)}[${name}${value != undefined ? `=${value}` : ""}]${selectedText}[/${name}]${textField.value.substring(selectionEnd, textField.value.length)}`
        textField.dispatchEvent(new Event("input", { bubbles: true }))

        // sets marked area
        textField.focus()
        textField.selectionStart = textField.value.substring(0, selectionStart).length + `[${name}${value != undefined ? `=${value}` : ""}]`.length
        textField.selectionEnd = textField.value.substring(0, selectionEnd).length + `[${name}${value != undefined ? `=${value}` : ""}]`.length
    }
}

/**
 * inserts a customMessage into the BBCode Editor
 * @param customMessage customMessage to insert
 */
export function insertForumMessage(customMessage: CustomMessage) {
    insertTextIntoBBCodeEditor(insertParametersIntoCustomMessage(customMessage), "div:not(.js-forum-post) textarea.bbcode-editor__body")
}

/**
 * inserts the button for forum customMessage modals
 */
function insertForumTemplateButton() {
    if (!location.pathname.includes("topics") || document.querySelector(".rr-lib.forum-templates") !== null) {
        return
    }

    insertStyleTag(`
    .forum-templates:hover{
        background-color: #382e32 !important
    }
    `)

    ensuredSelector(".bbcode-editor--reply .bbcode-editor__header").append(
        createElement("div", {
            className: "forum-templates",
            children: [createIcon("collection")],
            attributes: {
                title: "Forum Post Templates",
                onclick: () => {
                    const modalWrapper = new ModalWrapper()
                    modalWrapper.append(createMessageSelectModal(modalWrapper, "forum"))
                    document.body.append(modalWrapper)
                },
            },
            style: {
                padding: "4px",
                backgroundColor: "#46393f",
                cursor: "pointer",
                display: "flex",
                borderRadius: "4px",
            },
        })
    )
}

/**
 * Inserts text into the texteditor
 * if cursor is a simple cursor => text at position
 * if cursor is selection => replace selection
 * @param text text to insert
 * @param editor editor to use, defaults to first bbcode_editor__body
 */
function insertTextIntoBBCodeEditor(text: string, editor: string = ".bbcode-editor__body") {
    const textField = ensuredSelector<HTMLTextAreaElement>(editor)
    const selectionEnd = textField.selectionEnd
    const selectionStart = textField.selectionStart
    const cursorPosition = textField.value.substring(0, selectionEnd).length + text.length

    // value setting
    textField.value = textField.value.substring(0, selectionStart) + text + textField.value.substring(selectionEnd, textField.value.length)
    // update event + set cursor focus
    textField.dispatchEvent(new Event("input", { bubbles: true }))
    textField.focus()
    textField.selectionEnd = cursorPosition
    textField.selectionStart = cursorPosition
}
