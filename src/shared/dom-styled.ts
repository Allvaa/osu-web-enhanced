import { createElement } from "../../typescript-lib/src/shared/dom"
import { osuTheme } from "../../typescript-lib/src/shared/themes"

/**
 * This function creates a styled toggle input without an EventListener.
 * @param callback onClick function
 * @returns toggle
 */
export function createToggleInput(callback: Function = () => {}) {
    const toggle = createElement("div", {
        className: "toggle",
        style: {
            height: "14px",
            background: osuTheme.modalBackground,
            width: "14px",
            position: "absolute",
            top: "3px",
            right: "15px",
            borderRadius: "100%",
            transition: "200ms ease transform",
        },
    })
    const toggleBackground = createElement("div", {
        className: "toggle-bg",
        style: {
            height: "20px",
            background: osuTheme.secondaryBackground,
            width: "32px",
            borderRadius: "12px",
            border: `1px solid ${osuTheme.disabledColor}`,
            transition: "200ms ease background-color",
            display: "inline-block",
        },
    })
    const container = createElement("div", {
        className: "toggle-input",
        style: {
            display: "flex",
            position: "relative",
            cursor: "pointer",
        },
        children: [toggle, toggleBackground],
        attributes: {
            onclick: () => callback(),
        },
    })

    return container
}

/**
 * This function creates a styled textarea.
 * @returns The styled textarea.
 */
export function createTextAreaInput(classString: string, text: any) {
    return createElement("textarea", {
        className: `${classString} bbcode-editor__body`,
        attributes: {
            value: text,
            rows: 2,
        },
        style: {
            minHeight: "27px",
            backgroundColor: osuTheme.secondaryBackground,
            borderRadius: "4px",
            padding: "4px",
        },
    })
}
