import { ensuredSelector } from "../typescript-lib/src/shared/common"

/**
 * updates the document title to display number of notifications
 */
export function updateNotificationDisplay() {
    let chatCount = parseInt(ensuredSelector<HTMLSpanElement>(".fa-inbox + .notification-icon__count")?.innerText)
    let otherCount = parseInt(ensuredSelector<HTMLSpanElement>(".fa-comment-alt + .notification-icon__count")?.innerText)
    let totalCount = chatCount + otherCount

    const title = ensuredSelector<HTMLTitleElement>("title")

    if (chatCount == NaN) {
        chatCount = 0
    }
    if (otherCount == NaN) {
        otherCount = 0
    }

    if (title.innerText.match(new RegExp("✉ [0-9]* · .*")) != null && totalCount == 0) {
        title.innerText = title.innerText.replace(new RegExp("✉ [0-9]* · "), "")
    } else if (title.innerText.match(new RegExp("✉ [0-9]* · .*")) != null) {
        title.innerText = title.innerText.replace(new RegExp("✉ [0-9]* ·"), `✉ ${totalCount} ·`)
    } else if (totalCount > 0) {
        title.innerText = `✉ ${totalCount} · ${title.innerText}`
    }
}
