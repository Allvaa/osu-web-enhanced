import { ModalWrapper } from "../../typescript-lib/src/components/modal/ModalWrapper"
import { OsuModal } from "../../typescript-lib/src/components/modal/osu/OsuModal"
import { IconType } from "../../typescript-lib/src/constants/types"
import { createElement, createIcon } from "../../typescript-lib/src/shared/dom"
import { CustomMessage, customMessageTypes } from "../constants/types"
import { deleteCustomMessage } from "../template-messaging/storage"
import { createMessageEditor } from "../template-messaging/views/MessageEditor"
import { createMessageExport } from "../template-messaging/views/MessageExport"
import { createMessagesManager } from "../template-messaging/views/MessageManager"

/**
 * One entry in the message editors message list
 */
export class MessageEditorListItem extends HTMLDivElement {
    /**
     * This objects customMessage
     */
    private customMessage: CustomMessage

    /**
     * Modal to replace with navigation
     */
    private modal: OsuModal

    /**
     * Modal Wrapper to be inserted into
     */
    private modalWrapper: ModalWrapper

    /**
     * Type of the private message
     */
    private customMessageType: customMessageTypes

    /**
     * constructor
     * @param customMessage the customMessage to display in the modal
     * @param previousModal previous modal for replacement purposes
     * @param modalWrapper modalWrapper to be inserted into
     */
    constructor(customMessage: CustomMessage, previousModal: OsuModal, modalWrapper: ModalWrapper, customMessageType: customMessageTypes) {
        super()

        this.customMessage = customMessage
        this.modal = previousModal
        this.modalWrapper = modalWrapper
        this.customMessageType = customMessageType

        this.style.display = "flex"
        this.style.justifyContent = "space-between"
        this.style.alignItems = "center"

        this.append(
            createElement("span", {
                style: {
                    fontSize: "14px",
                    marginLeft: "8px",
                },
                attributes: {
                    innerText: this.customMessage.messageOptions.title,
                },
            }),
            createElement("div", {
                style: {
                    display: "flex",
                },
                children: [
                    this.createIconButton("upload", "Export Message", () => {
                        this.exportListener()
                    }),
                    this.createIconButton("edit", "Edit Message", () => {
                        this.editListener()
                    }),
                    this.createIconButton("trash", "Delete Message", () => {
                        this.deleteListener()
                    }),
                ],
            })
        )
    }

    /**
     * Creates each icon button for this list item
     * @param icon icon to be used, @see IconType
     * @param tooltip tooltip
     * @param eventFunction function for "click" EventListener
     * @returns the button
     */
    private createIconButton(icon: IconType, tooltip: string, eventFunction: () => void) {
        const button = createIcon(icon)
        button.style.marginLeft = "4px"
        button.style.cursor = "pointer"
        button.addEventListener("click", () => {
            eventFunction()
        })

        const container = createElement("div", {
            style: {
                display: "flex",
            },
            attributes: {
                title: tooltip,
            },
            children: [button],
        })

        return container
    }

    /**
     * listener for delete button
     */
    private deleteListener() {
        if (window.confirm(`Are you sure you want to delete ${this.customMessage.messageOptions.title}?`)) {
            deleteCustomMessage(this.customMessage)
            this.modal.replaceWith(createMessagesManager(this.modalWrapper, this.customMessageType))
        }
    }

    /**
     * listener for edit button
     */
    private editListener() {
        this.modal.replaceWith(createMessageEditor(this.modalWrapper, this.customMessage, "edit", this.customMessageType))
    }

    /**
     * listener for export button
     */
    private exportListener() {
        this.modal.replaceWith(createMessageExport(this.modalWrapper, this.customMessage, this.customMessageType))
    }
}
